package com.example.exception_handling.restcontroller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LocalExceptionHandling {

    @GetMapping("/demo1")
    public String demo1(){
        String msg=null;

        try{
        int a=10/0;
        msg= "Welcome to Ashok It";}

        catch (Exception e){
            throw  new ArithmeticException("Hey Don't Divide By Zero");
        }

        return  msg;
    }

    @ExceptionHandler(value = ArithmeticException.class)
    public ResponseEntity<String> handleArithmeticError(ArithmeticException e){
        return  new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
